#### Simple application for presentation list of Github users with details screen for each.

*Used technologies and patterns*
- Android clean architecture
- Android architecture components (MVVM, LiveData, Pagination)
- Retrofit
- RxJava
